import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class SlotTest {
    @Test
    fun shouldBeAbleToMakeNormalSlot() {
        val slot = Slot(SlotType.Normal)

        assertEquals(slot.type, SlotType.Normal)
    }

    @Test
    fun shouldBeAbleToMakePremiumSlot() {
        val slot = Slot(SlotType.Premium)

        assertEquals(slot.type, SlotType.Premium)
    }

    @Test
    fun isSlotIsEmptyWhenNoVehicleIsParked() {
        val slot = Slot(SlotType.Normal)

        assertFalse(slot.isOccupied())
    }

    @Test
    fun shouldBeAbleToParkAVehicleInASlot() {
        val slot = Slot(SlotType.Normal)
        slot.park(VehicleType.Car)

        assertEquals(VehicleType.Car, slot.vehicle)
    }

    @Test
    fun shouldGetNullWhenNoVehicleIsParkedInASlot() {
        val slot = Slot(SlotType.Normal)

        assertNull(slot.vehicle)
    }

    @Test
    fun shouldNotAbleToParkTruckAtPremiumSlot() {
        val slot = Slot(SlotType.Premium)

        assertFalse(slot.park(VehicleType.Truck))
    }

    @Test
    fun shouldTellIfBikeCanBeParkedAtPremiumSlot() {
        val slot = Slot(SlotType.Premium)

        assertFalse(slot.canPark(VehicleType.Bike))
    }

    @Test
    fun shouldTellEvCanBeParkedAtEvSlot() {
        val slot = Slot(SlotType.Ev)

        assertTrue(slot.canPark(VehicleType.Ev))
    }

    @Test
    fun shouldTellBikeCanBeParkedAtEvSlot() {
        val slot = Slot(SlotType.Ev)

        assertFalse(slot.canPark(VehicleType.Bike))
    }

    @Test
    fun shouldBeAbleToParkCarAtPremiumSlot() {
        val slot = Slot(SlotType.Premium)

        assertTrue(slot.park(VehicleType.Car))
    }

    @Test
    fun shouldGetNullWhenVehicleIsUnParked() {
        val slot = Slot(SlotType.Normal)
        slot.park(VehicleType.Car)
        slot.unPark()

        assertNull(slot.vehicle)
    }

    @Test
    fun isSlotIsOccupiedWhenVehicleIsParked() {
        val slot = Slot(SlotType.Normal)
        slot.park(VehicleType.Car)

        assertTrue(slot.isOccupied())
    }

    @Test
    fun shouldNotLetParkOverParkedVehicle() {
        val slot = Slot(SlotType.Normal)
        slot.park(VehicleType.Car)

        assertFalse(slot.park(VehicleType.Car))
    }

    @Test
    fun shouldNotBeAbleToUnParkEmptySlot() {
        val slot = Slot(SlotType.Normal)

        val result = slot.unPark()

        assertNull(result)
    }
}
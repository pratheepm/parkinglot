import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach

import org.junit.jupiter.api.Test


class ParkingLotTest {
    private lateinit var parkingLot: ParkingLot

    @BeforeEach
    fun init() {
        parkingLot = ParkingLot(100)
    }

    @Test
    fun shouldBeAbleToParkAVehicle() {
        val receipt = parkingLot.park(SlotType.Normal, VehicleType.Car)


        val expected = Receipt(LocalTime.now(), SlotType.Normal, VehicleType.Car, 0, 1)
        assertEquals(expected, receipt)
    }

    @Test
    fun shouldBeAbleToUnParkVehicle() {
        val parkingLot = ParkingLot(100)
        val receipt = parkingLot.park(SlotType.Premium, VehicleType.Car)
        LocalTime.addHour(1)
        val bill = receipt?.let { parkingLot.unPark(it) }

        val expected = receipt?.let { Bill(it.parkingTime, LocalTime.now(), SlotType.Premium, VehicleType.Car, 20.0) }

        assertEquals(expected, bill)
    }


    @Test
    fun shouldNotAbleToFindSlotIfNothingIsEmpty() {
        for (i in 0 until 50) {
            parkingLot.park(SlotType.Normal, VehicleType.Car)
        }

        assertNull(parkingLot.findIndexToParkVehicle(SlotType.Normal, VehicleType.Car))
    }

    @Test
    fun shouldNotLetBikeParkAtPremiumSlot() {
        val receipt = parkingLot.park(SlotType.Premium, VehicleType.Bike)

        assertNull(receipt)
    }

    @Test
    fun shouldLetCarParkedAtPremiumSlot() {
        val receipt = parkingLot.park(SlotType.Premium, VehicleType.Car)

        assertNotNull(receipt)
    }

    @Test
    fun shouldAbleToParkMultipleVehicle() {
        parkingLot.park(SlotType.Normal, VehicleType.Bike)
        parkingLot.park(SlotType.Ev, VehicleType.Ev)
        parkingLot.park(SlotType.Normal, VehicleType.Truck)

        val receipt = parkingLot.park(SlotType.Normal, VehicleType.Bike)

        assertNotNull(receipt)
    }

    @Test
    fun shouldBeAbleToParkUnParkThenParkVehicle() {
        val receipt1 = parkingLot.park(SlotType.Normal, VehicleType.Bike)

        assertNotNull(receipt1)

        receipt1?.let { parkingLot.unPark(it) }
        val receipt = parkingLot.park(SlotType.Normal, VehicleType.Bike)

        assertNotNull(receipt)
    }
}
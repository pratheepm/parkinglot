enum class SlotType(val price: Int, val capacityRatio: Float, val permittedVehicleType: List<VehicleType>) {
    Normal(10, 0.8f, listOf(VehicleType.Bike, VehicleType.Car, VehicleType.Truck)),
    Premium(20, 0.1f, listOf(VehicleType.Car)),
    Ev(5, 0.1f, listOf(VehicleType.Ev))
}
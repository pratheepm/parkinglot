import java.time.LocalTime

data class Bill(
    val parkingTime: LocalTime,
    val unParkingTime: LocalTime,
    val slotType: SlotType,
    val vehicleType: VehicleType,
    val amount: Double
)
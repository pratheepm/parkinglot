class Slot(

    var type: SlotType
) {
    var vehicle: VehicleType? = null
    fun park(vehicle: VehicleType): Boolean {
        if (isOccupied() || !canPark(vehicle)) {
            return false
        }
        this.vehicle = vehicle
        return true
    }

    fun isOccupied(): Boolean {
        return vehicle != null
    }

    fun unPark(): VehicleType? {
        val result = this.vehicle
        this.vehicle = null
        return result
    }

    fun canPark(vehicle: VehicleType): Boolean {
        return type.permittedVehicleType.contains(vehicle)
    }

}
import java.time.LocalTime

data class Receipt(
    val parkingTime: LocalTime,
    val slotType: SlotType,
    val vehicleType: VehicleType,
    val startingIndex: Int,
    val endingTime: Int
)
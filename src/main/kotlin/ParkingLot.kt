class ParkingLot(capacity: Int) {
    private val slots = mutableListOf<Slot>()

    init {
        enumValues<SlotType>().forEach {
            val size = capacity * it.capacityRatio
            addSlotOfType(it, size.toInt())
        }
    }

    fun findIndexToParkVehicle(slotType: SlotType, vehicle: VehicleType): Int? {

        for (i in slots.indices) {
            if (slots[i].type == slotType && slots[i].canPark(vehicle)) {
                val result = isAbleToFitVehicleAtIndex(i, vehicle)
                if (result) return i
            }
        }
        return null
    }

    fun park(slotType: SlotType, vehicle: VehicleType): Receipt? {
        val startingIndex = findIndexToParkVehicle(slotType, vehicle) ?: return null
        val endingIndex = startingIndex + vehicle.slotRequired - 1
        makeSlotParked(startingIndex, endingIndex, vehicle)
        return Receipt(LocalTime.now(), slotType, vehicle, startingIndex, endingIndex)
    }

    fun unPark(receipt: Receipt): Bill {
        val startingIndex = receipt.startingIndex
        val endingIndex = startingIndex + receipt.vehicleType.slotRequired - 1
        makeSlotUnParked(startingIndex, endingIndex)
        val parkingTime = receipt.parkingTime
        val currentTime = LocalTime.now()
        val amount = calculatePaymentAmount(receipt.slotType, parkingTime, currentTime)

        return Bill(parkingTime, currentTime, receipt.slotType, receipt.vehicleType, amount)
    }

    private fun addSlotOfType(type: SlotType, size: Int) {
        for (i in 0 until size) {
            slots.add(Slot(type))
        }
    }

    private fun isAbleToFitVehicleAtIndex(index: Int, vehicle: VehicleType): Boolean {
        var found = true
        for (i in index until index + vehicle.slotRequired) {
            if (slots[i].isOccupied()) {
                found = false
            }
        }
        return found
    }

    private fun makeSlotParked(startingIndex: Int, endingIndex: Int, vehicleType: VehicleType) {
        for (i in startingIndex..endingIndex) {
            slots[i].park(vehicleType)
        }
    }

    private fun makeSlotUnParked(startingIndex: Int, endingIndex: Int) {
        for (i in startingIndex..endingIndex) {
            slots[i].unPark()
        }
    }

    private fun calculatePaymentAmount(
        slotType: SlotType, parkingTime: java.time.LocalTime, unParkingTime: java.time.LocalTime
    ): Double {
        val duration: Double =
            (unParkingTime.hour - parkingTime.hour).toDouble() + (unParkingTime.minute - parkingTime.minute).toDouble() / 60
        return slotType.price * duration
    }
}
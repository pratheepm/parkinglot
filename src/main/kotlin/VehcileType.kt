enum class VehicleType(val slotRequired: Int) {
    Car(2), Bike(1), Truck(4), Ev(2)
}
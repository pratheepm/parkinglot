class LocalTime {
    companion object {
        private var time: java.time.LocalTime = java.time.LocalTime.now()

        fun now(): java.time.LocalTime {
            return time

        }

        fun addHour(hour: Long) {
            time = time.plusHours(hour)
        }
    }
}